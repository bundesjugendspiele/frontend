# Build
FROM node:16 AS build

WORKDIR /app

COPY . .

RUN npm install --legacy-peer-deps
RUN npm run build

EXPOSE 80



# Prod
FROM build AS prod

WORKDIR /var/www/html

RUN apt-get update
RUN apt-get install -y nginx

COPY --from=build /app/dist .

CMD [ "nginx", "-g", "daemon off;" ]



# Dev
FROM build AS dev

WORKDIR /app

VOLUME [ "/app" ]

CMD [ "sh", "-c", "npm install --legacy-peer-deps && npm run serve -- --port 80" ]
