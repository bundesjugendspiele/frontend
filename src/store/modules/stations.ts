import { ActionContext, Module } from 'vuex';
import { IRootState } from '@/store';
import {
	createStation,
	deleteStation,
	getAllStations,
	getStationById,
	PaginatedPayload,
	paginationDefault,
	updateStation,
} from '@/services/apiService';
import { Unit } from '.';

export interface Station {
	id: number;
	name: string;
	unitId: number;
	unit: Unit | null;
	createdAt: string;
}

export interface StationsState {
	stations: Station[];
	currentStation: Station | null;
}

export const stationsModule: Module<StationsState, IRootState> = {
	state: (): StationsState => ({
		stations: [],
		currentStation: null,
	}),
	mutations: {
		setStations(state: StationsState, stations: Station[]): void {
			state.stations = stations;
		},
		setCurrentStation(
			state: StationsState,
			newStation: Station | null
		): void {
			state.currentStation = newStation;
		},
		updateStation(state: StationsState, updatedStation: Station): void {
			const index = state.stations.findIndex(
				(station) => station.id === updatedStation.id
			);

			if (index > -1)
				state.stations[index] = {
					...state.stations[index],
					...updatedStation,
				};
		},
	},
	actions: {
		async fetchStations(
			context: ActionContext<StationsState, IRootState>,
			payload: PaginatedPayload | undefined
		): Promise<void> {
			context.commit(
				'setStations',
				await getAllStations(payload ?? paginationDefault)
			);
		},
		async fetchStationById(
			context: ActionContext<StationsState, IRootState>,
			payload: number
		): Promise<void> {
			context.commit('setCurrentStation', await getStationById(payload));
		},
		setCurrentStation(
			context: ActionContext<StationsState, IRootState>,
			payload: Station
		): void {
			context.commit('setCurrentStation', payload);
		},
		async updateStation(
			context: ActionContext<StationsState, IRootState>,
			payload: Station
		): Promise<void> {
			const newStation = await updateStation(payload);

			context.commit('updateStation', newStation);
		},
		async createStation(
			context: ActionContext<StationsState, IRootState>,
			payload: Station
		): Promise<void> {
			await createStation(payload);

			context.dispatch('fetchStations');
		},
		async deleteStation(
			context: ActionContext<StationsState, IRootState>,
			payload: number
		): Promise<void> {
			await deleteStation(payload);

			context.dispatch('fetchStations');
		},
	},
	getters: {
		getStations(state: StationsState): Station[] {
			return state.stations;
		},
		getSortedStations(state: StationsState): Station[] {
			return state.stations.sort(
				(a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt)
			);
		},
		getCurrentStation(state: StationsState): Station | null {
			return state.currentStation;
		},
	},
};
