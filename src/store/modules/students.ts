import { ActionContext, Module } from 'vuex';
import { IRootState } from '@/store';
import {
	createStudent,
	deleteStudent,
	getAllStudents,
	updateStudent,
	PaginatedPayload,
	paginationDefault,
	getStudentById,
} from '@/services/apiService';

export interface Student {
	id: number;
	firstName: string;
	lastName: string;
	createdAt: string;
}

export interface StudentsState {
	students: Student[];
	currentStudent: Student | null;
}

export const studentsModule: Module<StudentsState, IRootState> = {
	state: (): StudentsState => ({
		students: [],
		currentStudent: null,
	}),
	mutations: {
		setStudents(state: StudentsState, students: Student[]): void {
			state.students = students;
		},
		setCurrentStudent(
			state: StudentsState,
			newStudent: Student | null
		): void {
			state.currentStudent = newStudent;
		},
		updateStudent(state: StudentsState, updatedStudent: Student): void {
			const index = state.students.findIndex(
				(student) => student.id === updatedStudent.id
			);

			if (index > -1)
				state.students[index] = {
					...state.students[index],
					...updatedStudent,
				};
		},
	},
	actions: {
		async fetchStudents(
			context: ActionContext<StudentsState, IRootState>,
			payload: PaginatedPayload | undefined
		): Promise<void> {
			context.commit(
				'setStudents',
				await getAllStudents(payload ?? paginationDefault)
			);
		},
		async fetchStudentById(
			context: ActionContext<StudentsState, IRootState>,
			payload: number
		): Promise<void> {
			context.commit('setCurrentStudent', await getStudentById(payload));
		},
		setCurrentStudent(
			context: ActionContext<StudentsState, IRootState>,
			payload: Student
		): void {
			context.commit('setCurrentStudent', payload);
		},
		async updateStudent(
			context: ActionContext<StudentsState, IRootState>,
			payload: Student
		): Promise<void> {
			const newStudent = await updateStudent(payload);

			context.commit('updateStudent', newStudent);
		},
		async createStudent(
			context: ActionContext<StudentsState, IRootState>,
			payload: Student
		): Promise<void> {
			await createStudent(payload);

			context.dispatch('fetchStudents');
		},
		async deleteStudent(
			context: ActionContext<StudentsState, IRootState>,
			payload: number
		): Promise<void> {
			await deleteStudent(payload);

			context.dispatch('fetchStudents');
		},
	},
	getters: {
		getStudents(state: StudentsState): Student[] {
			return state.students;
		},
		getSortedStudents(state: StudentsState): Student[] {
			return state.students.sort(
				(a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt)
			);
		},
		getCurrentStudent(state: StudentsState): Student | null {
			return state.currentStudent;
		},
	},
};
