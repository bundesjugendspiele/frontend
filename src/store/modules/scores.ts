import { ActionContext, Module } from 'vuex';
import { IRootState } from '@/store';
import {
	createScore,
	deleteScore,
	getAllScores,
	updateScore,
	PaginatedPayload,
	paginationDefault,
} from '@/services/apiService';
import { Station, Student } from '.';

export interface Score {
	score: number;
	stationId: number;
	station: Station | null;
	studentId: number;
	student: Student | null;
	createdAt: string;
}

export interface ScoresState {
	scores: Score[];
	currentScore: Score | null;
}

export const scoreModule: Module<ScoresState, IRootState> = {
	state: (): ScoresState => ({
		scores: [],
		currentScore: null,
	}),
	mutations: {
		setScores(state: ScoresState, scores: Score[]): void {
			state.scores = scores;
		},
		setCurrentScore(state: ScoresState, newScore: Score | null): void {
			state.currentScore = newScore;
		},
		updateScore(state: ScoresState, updatedScore: Score): void {
			const index = state.scores.findIndex(
				(score) =>
					score.studentId === updatedScore.studentId &&
					score.stationId === updatedScore.stationId
			);

			if (index > -1)
				state.scores[index] = {
					...state.scores[index],
					...updatedScore,
				};
		},
	},
	actions: {
		async fetchScores(
			context: ActionContext<ScoresState, IRootState>,
			payload: PaginatedPayload | undefined
		): Promise<void> {
			context.commit(
				'setScores',
				await getAllScores(payload ?? paginationDefault)
			);
		},
		setCurrentScore(
			context: ActionContext<ScoresState, IRootState>,
			payload: Score
		): void {
			context.commit('setCurrentScore', payload);
		},
		async updateScore(
			context: ActionContext<ScoresState, IRootState>,
			payload: Score
		): Promise<void> {
			const newScore = await updateScore(payload);

			context.commit('updateScore', newScore);
		},
		async createScore(
			context: ActionContext<ScoresState, IRootState>,
			payload: Score
		): Promise<void> {
			await createScore(payload);

			context.dispatch('fetchScores');
		},
		async deleteScore(
			context: ActionContext<ScoresState, IRootState>,
			payload: Score
		): Promise<void> {
			await deleteScore(payload);

			context.dispatch('fetchScores');
		},
	},
	getters: {
		getScores(state: ScoresState): Score[] {
			return state.scores;
		},
		getSortedScores(state: ScoresState): Score[] {
			return state.scores.sort(
				(a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt)
			);
		},
		getCurrentScore(state: ScoresState): Score | null {
			return state.currentScore;
		},
	},
};
