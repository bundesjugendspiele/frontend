import { ActionContext, Module } from 'vuex';
import { IRootState } from '@/store';
import { VNode } from 'vue';

export interface ModalState {
	modalOpen: boolean;
	content: VNode | null;
}

export const modalModule: Module<ModalState, IRootState> = {
	state: (): ModalState => ({
		modalOpen: false,
		content: null,
	}),
	mutations: {
		setContent(state: ModalState, newContent: VNode | null): void {
			state.content = newContent;
		},
		setModalOpen(state: ModalState, newState: boolean): void {
			state.modalOpen = newState;
		},
	},
	actions: {
		openModal(
			context: ActionContext<ModalState, IRootState>,
			content: VNode
		): void {
			context.commit('setContent', content);
			context.commit('setModalOpen', true);
		},
		closeModal(context: ActionContext<ModalState, IRootState>): void {
			context.commit('setModalOpen', false);
		},
	},
	getters: {
		getContent(state: ModalState): VNode | null {
			return state.content;
		},
		getModalOpen(state: ModalState): boolean {
			return state.modalOpen;
		},
	},
};
