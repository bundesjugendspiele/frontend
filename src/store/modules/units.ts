import { ActionContext, Module } from 'vuex';
import { IRootState } from '@/store';
import {
	createUnit,
	deleteUnit,
	getAllUnits,
	updateUnit,
	PaginatedPayload,
	paginationDefault,
} from '@/services/apiService';

export interface Unit {
	id: number;
	nameSingular: string;
	namePlural: string;
	createdAt: string;
}

export interface UnitsState {
	units: Unit[];
	currentUnit: Unit | null;
}

export const unitsModule: Module<UnitsState, IRootState> = {
	state: (): UnitsState => ({
		units: [],
		currentUnit: null,
	}),
	mutations: {
		setUnits(state: UnitsState, units: Unit[]): void {
			state.units = units;
		},
		setCurrentUnit(state: UnitsState, newUnit: Unit | null): void {
			state.currentUnit = newUnit;
		},
		updateUnit(state: UnitsState, updatedUnit: Unit): void {
			const index = state.units.findIndex(
				(unit) => unit.id === updatedUnit.id
			);

			if (index > -1)
				state.units[index] = { ...state.units[index], ...updatedUnit };
		},
	},
	actions: {
		async fetchUnits(
			context: ActionContext<UnitsState, IRootState>,
			payload: PaginatedPayload | undefined
		): Promise<void> {
			context.commit(
				'setUnits',
				await getAllUnits(payload ?? paginationDefault)
			);
		},
		setCurrentUnit(
			context: ActionContext<UnitsState, IRootState>,
			payload: Unit
		): void {
			context.commit('setCurrentUnit', payload);
		},
		async updateUnit(
			context: ActionContext<UnitsState, IRootState>,
			payload: Unit
		): Promise<void> {
			const newUnit = await updateUnit(payload);

			context.commit('updateUnit', newUnit);
		},
		async createUnit(
			context: ActionContext<UnitsState, IRootState>,
			payload: Unit
		): Promise<void> {
			await createUnit(payload);

			context.dispatch('fetchUnits');
		},
		async deleteUnit(
			context: ActionContext<UnitsState, IRootState>,
			payload: number
		): Promise<void> {
			await deleteUnit(payload);

			context.dispatch('fetchUnits');
		},
	},
	getters: {
		getUnits(state: UnitsState): Unit[] {
			return state.units;
		},
		getSortedUnits(state: UnitsState): Unit[] {
			return state.units.sort(
				(a, b) => Date.parse(b.createdAt) - Date.parse(a.createdAt)
			);
		},
		getCurrentUnit(state: UnitsState): Unit | null {
			return state.currentUnit;
		},
	},
};
