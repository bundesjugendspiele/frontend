import { createStore } from 'vuex';
import {
	studentsModule,
	unitsModule,
	modalModule,
	stationsModule,
	scoreModule,
} from './modules';

export interface IRootState {
	root: boolean;
	version: string;
}

export default createStore({
	modules: {
		students: studentsModule,
		units: unitsModule,
		stations: stationsModule,
		scores: scoreModule,
		modal: modalModule,
	},
});
