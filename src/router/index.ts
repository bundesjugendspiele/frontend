import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import StudentsOverview from '../views/StudentsOverview.vue';
import StudentDetails from '../views/StudentDetails.vue';
import StationsOverview from '../views/StationsOverview.vue';
import StationDetails from '../views/StationDetails.vue';
import UnitsOverview from '../views/UnitsOverview.vue';
import ScoresOverview from '../views/ScoresOverview.vue';

const routes: Array<RouteRecordRaw> = [
	{
		path: '/',
		name: 'Home',
		component: StationsOverview,
	},
	{
		path: '/students',
		name: 'Students Overview',
		component: StudentsOverview,
	},
	{
		path: '/students/:studentId',
		name: 'Student Details',
		component: StudentDetails,
	},
	{
		path: '/stations',
		name: 'Stations Overview',
		component: StationsOverview,
	},
	{
		path: '/stations/:stationId',
		name: 'Station Details',
		component: StationDetails,
	},
	{
		path: '/units',
		name: 'Units Overview',
		component: UnitsOverview,
	},
	{
		path: '/scores',
		name: 'Scores Overview',
		component: ScoresOverview,
	},
];

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});

export default router;
