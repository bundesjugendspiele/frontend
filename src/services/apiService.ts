import { Student, Unit, Station, Score } from '@/store/modules';

const backendUrl = '/api';

interface PaginatedResponse<S> {
	data: S[];
	total: number;
}

export interface PaginatedPayload {
	page: number;
	limit: number;
}

export const paginationDefault = <PaginatedPayload>{
	page: 0,
	limit: 5,
};

// Student

export const getAllStudents = async ({
	page,
	limit,
}: PaginatedPayload): Promise<Student[]> => {
	const response = (await (
		await fetch(`${backendUrl}/students?page=${page}&per_page=${limit}`)
	).json()) as PaginatedResponse<Student>;

	return response.data;
};

export const getStudentById = async (id: number): Promise<Student | null> => {
	const response = (await (
		await fetch(`${backendUrl}/students/${id}`)
	).json()) as Student | null;

	return response;
};

export const updateStudent = async (payload: Student): Promise<Student> => {
	const { id, firstName, lastName } = payload;
	const response = (await (
		await fetch(`${backendUrl}/students/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ firstName, lastName }),
		})
	).json()) as Student;

	return response;
};

export const createStudent = async (payload: Student): Promise<Student> => {
	const { firstName, lastName } = payload;
	const response = (await (
		await fetch(`${backendUrl}/students`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ firstName, lastName }),
		})
	).json()) as Student;

	return response;
};

export const deleteStudent = async (id: number): Promise<void> => {
	await fetch(`${backendUrl}/students/${id}`, {
		method: 'DELETE',
	});
};

// Unit

export const getAllUnits = async ({
	page,
	limit,
}: PaginatedPayload): Promise<Unit[]> => {
	const response = (await (
		await fetch(`${backendUrl}/units?page=${page}&per_page=${limit}`)
	).json()) as PaginatedResponse<Unit>;

	return response.data;
};

export const getUnitById = async (id: number): Promise<Unit | null> => {
	const response = (await (
		await fetch(`${backendUrl}/units/${id}`)
	).json()) as Unit | null;

	return response;
};

export const updateUnit = async (payload: Unit): Promise<Unit> => {
	const { id, nameSingular, namePlural } = payload;
	const response = (await (
		await fetch(`${backendUrl}/units/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ nameSingular, namePlural }),
		})
	).json()) as Unit;

	return response;
};

export const createUnit = async (payload: Unit): Promise<Unit> => {
	const { nameSingular, namePlural } = payload;
	const response = (await (
		await fetch(`${backendUrl}/units`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ nameSingular, namePlural }),
		})
	).json()) as Unit;

	return response;
};

export const deleteUnit = async (id: number): Promise<void> => {
	await fetch(`${backendUrl}/stations/${id}`, {
		method: 'DELETE',
	});
};

// Station

export const getAllStations = async ({
	page,
	limit,
}: PaginatedPayload): Promise<Station[]> => {
	const response = (await (
		await fetch(`${backendUrl}/stations?page=${page}&per_page=${limit}`)
	).json()) as PaginatedResponse<Station>;

	return response.data;
};

export const getStationById = async (id: number): Promise<Station | null> => {
	const response = (await (
		await fetch(`${backendUrl}/stations/${id}`)
	).json()) as Station | null;

	return response;
};

export const updateStation = async (payload: Station): Promise<Station> => {
	const { id, name, unitId } = payload;
	const response = (await (
		await fetch(`${backendUrl}/stations/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ name, unitId }),
		})
	).json()) as Station;

	return response;
};

export const createStation = async (payload: Station): Promise<Station> => {
	const { name, unitId } = payload;
	const response = (await (
		await fetch(`${backendUrl}/stations`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ name, unitId }),
		})
	).json()) as Station;

	return response;
};

export const deleteStation = async (id: number): Promise<void> => {
	await fetch(`${backendUrl}/stations/${id}`, {
		method: 'DELETE',
	});
};

// Score

export const getAllScores = async ({
	page,
	limit,
}: PaginatedPayload): Promise<Score[]> => {
	const response = (await (
		await fetch(`${backendUrl}/scores?page=${page}&per_page=${limit}`)
	).json()) as PaginatedResponse<Score>;

	return response.data;
};

export const getScoreById = async (
	stationId: number,
	studentId: number
): Promise<Score | null> => {
	const response = (await (
		await fetch(`${backendUrl}/scores/${stationId}/${studentId}`)
	).json()) as Score | null;

	return response;
};

export const updateScore = async (payload: Score): Promise<Score> => {
	const { score, studentId, stationId } = payload;
	const response = (await (
		await fetch(`${backendUrl}/scores/${stationId}/${studentId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ score }),
		})
	).json()) as Score;

	return response;
};

export const createScore = async (payload: Score): Promise<Score> => {
	const { stationId, studentId, score } = payload;
	const response = (await (
		await fetch(`${backendUrl}/scores`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({ stationId, studentId, score }),
		})
	).json()) as Score;

	return response;
};

export const deleteScore = async (score: Score): Promise<void> => {
	await fetch(`${backendUrl}/scores/${score.stationId}/${score.studentId}`, {
		method: 'DELETE',
	});
};

// Other

export const searchForItem = async (
	itemKey: string,
	query: string
): Promise<unknown[]> => {
	const response = (await (
		await fetch(`${backendUrl}/${itemKey}?${query}`)
	).json()) as PaginatedResponse<unknown>;

	return response.data;
};
